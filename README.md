# open-regels-api

## K8s Deployment (using helm)

**!Note** The helm chart in this repository pulls from docker hub. The actual image is built in a different CI/CD pipeline here:
https://github.com/sjefvanleeuwen/morstead/tree/master/src/Vs.Rules.OpenApi

This is just to test the API in order to make it easy to get it into the Haven App Store.

### Install

```bash
$ helm install open-regels-api ./chart/
```

### Check Install

```bash
$ kubectl get all --selector app=open-regels-api
NAME                                              READY   STATUS    RESTARTS   AGE
pod/open-regels-api-deployment-86d874dcf8-zbzhb   1/1     Running   0          7m22s
NAME                              TYPE        CLUSTER-IP      EXTERNAL-IP   PORT(S)    AGE
service/open-regels-api-service   ClusterIP   10.104.37.239   <none>        8888/TCP   7m22s
NAME                                         READY   UP-TO-DATE   AVAILABLE   AGE
deployment.apps/open-regels-api-deployment   1/1     1            1           7m22s
NAME                                                    DESIRED   CURRENT   READY   AGE
replicaset.apps/open-regels-api-deployment-86d874dcf8   1         1         1       7m22s
```

### Expose

If you don't deploy using an ingress, you can easily expose the API service for dev purposes:

```bash
$ kubectl port-forward service/open-regels-api-service 5088:5088
```

### Verify

Point your browser to: http://localhost:5088/

![Swagg](./doc/img/swagg.png)

### Uninstall

```bash
$ helm uninstall open-regels-api
```

